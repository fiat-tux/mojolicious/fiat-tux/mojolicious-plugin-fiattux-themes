package Mojolicious::Plugin::FiatTux::Themes;
use Mojo::Base 'Mojolicious::Plugin';

our $VERSION = '0.02';

sub register {
    my ($self, $app) = @_;

    # Themes handling
    shift @{$app->static->paths};
    shift @{$app->renderer->paths};
    if ($app->config('theme') ne 'default') {
        my $theme = $app->home->rel_file('themes/'.$app->config('theme'));
        push @{$app->static->paths},   $theme.'/public' if -d $theme.'/public';
        push @{$app->renderer->paths}, $theme.'/templates' if -d $theme.'/templates';
    }
    push @{$app->static->paths},   $app->home->rel_file('themes/default/public');
    push @{$app->renderer->paths}, $app->home->rel_file('themes/default/templates');
}

1;
__END__

=encoding utf8

=head1 NAME

Mojolicious::Plugin::FiatTux::Themes - Mojolicious Plugin

=head1 SYNOPSIS

  # Mojolicious
  $self->plugin('FiatTux::Themes');

  # Mojolicious::Lite
  plugin 'FiatTux::Themes';

=head1 DESCRIPTION

L<Mojolicious::Plugin::FiatTux::Themes> is a L<Mojolicious> plugin.

=head1 METHODS

L<Mojolicious::Plugin::FiatTux::Themes> inherits all methods from
L<Mojolicious::Plugin> and implements the following new ones.

=head2 register

  $plugin->register(Mojolicious->new);

Register plugin in L<Mojolicious> application.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Guides>, L<https://mojolicious.org>.

=cut
